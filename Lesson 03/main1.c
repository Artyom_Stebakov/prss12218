#include <stdio.h>

float celsToFahr(float in)
{
	return (in * 1.8f) + 32;
}

float fahrToCels(float in)
{
	return (in - 32) / 1.8f;
}

void main1()
{
	float number;

	printf("Enter degrees Celsius:\n");
	scanf_s("%f", &number);
	printf("%f degrees Celsius is %f Fahrenheits.\n", number, celsToFahr(number));

	printf("Enter Fahrenheits:\n");
	scanf_s("%f", &number);
	printf("%f Fahrenheits is %f degrees Celsius.\n", number, fahrToCels(number));

	getch();
}