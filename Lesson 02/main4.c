#include <stdio.h>

int main()
{
	float first;
	float second;

	printf("Enter numbers!\n");
	scanf_s("%f %f", &first, &second);
	printf("%f + %f = %f\n", first, second, first + second);
	printf("%f - %f = %f\n", first, second, first - second);
	printf("%f * %f = %f\n", first, second, first * second);
	printf("%f / %f = %f\n", first, second, first / second);

	getch();
	return 0;
}